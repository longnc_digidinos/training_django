from django import forms
from .models import Course

class InputForm(forms.ModelForm):
    title = forms.CharField(label='Name')
    page = forms.IntegerField(label='Pages')

    #
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
    #
    # def __init__(self, course):
    #     self.title = course.title
    #     self.page = course.page
    #     super().__init__()

    def __init__(self, *args, **kwargs):
        self.title = kwargs.pop('title', None)
        self.page = kwargs.pop('page', None)
        super().__init__(*args, **kwargs)
    def save(self, commit=True):
        course = super().save(commit=False)
        course.title = self.title
        course.page = self.page
        course.save()
    class Meta:
        model = Course
        fields = ["title"]