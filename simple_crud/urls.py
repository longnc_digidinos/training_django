from django.urls import path
from . import views

urlpatterns = [
    path('', views.list),
    path('add_new', views.add_new, name='add_new'),
    path('edit/<int:id>', views.edit, name='edit'),
    path('delete/<int:id>', views.delete, name='delete')
]