from django.db import models

# Create your models here.
class Course(models.Model):
    title = models.TextField()
    page = models.PositiveIntegerField()

    def __str__(self):
        return f'{self.title} ({self.page} Pages)'