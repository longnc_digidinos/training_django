from django.shortcuts import render
from .models import Course
from .forms import InputForm
from django.http import HttpResponseRedirect

# Create your views here.
def list(request):
    return render(request, 'list.html', {'list': Course.objects.all()})
def add_new(request):
    form = InputForm()
    if request.method == 'POST':
        form = InputForm(request.POST, title=request.POST['title'], page=request.POST['page'])
        form.save()
        return HttpResponseRedirect('/course')
    return render(request, 'add_new.html', {'form': form, 'title': 'add_new'})
def delete(request, id):
    if request.method == 'POST':
        course = Course(id=id)
        course.delete()
        return HttpResponseRedirect('/course')
    return render(request, 'delete.html', {'course': Course.objects.filter(id=id).first()})
def edit(request, id):
    form = InputForm(initial={'title':Course.objects.filter(id=id).first().title, 'page':Course.objects.filter(id=id).first().page})
    if request.method == 'POST':
        course = Course.objects.get(id=id)
        course.title = request.POST['title']
        course.page = request.POST['page']
        course.save()
        return HttpResponseRedirect('/course')
    return render(request, 'add_new.html', {'form': form, 'title': 'edit', 'id':id})